# tf-k8s

Terraform basic script for create a AKS instance

## Configure the parameters
Create a new file terraform.tfvars with the variable's values.

Example:

```bash
# Content of terraform.tfvars
project = "demokube"
vm_size = "Standard_B2ms"
```

## Launch the script

```bash
terraform init
terraform plan -out ".terraform/tfplan"
terraform apply tfplan ".terraform/tfplan"
```

**Attention!!!** the terraform state is storage in local.
{: .alert .alert-info}

For storage the state in a **Azure Storage Account** view the documentation link [Store Terraform state in Azure Storage](https://dep-docs.apps.ocp4.innershift.sodigital.io/devops-tools/iac/azure_stack/terraform_backend/)

# Autodevops configuration - Existing Kubernetes cluster

To add a Kubernetes cluster to your project, group, or instance:

1. Navigate to your:

    1. Project's {cloud-gear} Operations > Kubernetes page, for a project-level cluster.
    2. Group's {cloud-gear} Kubernetes page, for a group-level cluster.
    3. {admin} Admin Area > {cloud-gear} Kubernetes page, for an instance-level cluster.

2. Click **Add Kubernetes cluster**.

3. Click the **Add existing cluster** tab and fill in the details:

    1. **Kubernetes cluster name (required)** - The name you wish to give the cluster.

    2. **Environment scope (required)** - The associated environment to this cluster.

    3. **API URL (required)** - It's the URL that GitLab uses to access the Kubernetes API. Kubernetes exposes several APIs, we want the "base" URL that is common to all of them

    Get the API URL by running this command:

    ```bash
    kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
    ```

    4. **CA certificate (required)** - A valid Kubernetes certificate is needed to authenticate to the cluster. We will use the certificate created by default.

        1. List the secrets with `kubectl get secrets`, and one should be named similar to `default-token-xxxxx`. Copy that token name for use below.
    
        2. Get the certificate by running this command:

        ```bash
        kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
        ```

        **NOTE**: If the command returns the entire certificate chain, you need copy the *root ca* certificate at the bottom of the chain.

    5. **Token** - GitLab authenticates against Kubernetes using service tokens, which are scoped to a particular `namespace`. **The token used should belong to a service account with `cluster-admin` privileges**. To create this service account:

        1. Create a file called `gitlab-admin-service-account.yaml` with contents:

        ```yaml
        apiVersion: v1
        kind: ServiceAccount
        metadata:
        name: gitlab-admin
        namespace: kube-system
        ---
        apiVersion: rbac.authorization.k8s.io/v1beta1
        kind: ClusterRoleBinding
        metadata:
        name: gitlab-admin
        roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: cluster-admin
        subjects:
        - kind: ServiceAccount
            name: gitlab-admin
            namespace: kube-system
        ```

        2. Apply the service account and cluster role binding to your cluster:

        ```bash
        kubectl apply -f gitlab-admin-service-account.yaml
        ```

        You will need the `container.clusterRoleBindings.create` permission to create cluster-level roles. If you do not have this permission, you can alternatively enable Basic Authentication and the run the `kubectl apply` command as an admin:

        ```bash
        kubectl apply -f gitlab-admin-service-account.yaml --username=admin --password=<password>
        ```

        **NOTE**: Basic Authentication can be turned on and the password credentials can be obtained using the Google Cloud Console.

        Output:

        ```bash
        serviceaccount/gitlab-admin created
        clusterrolebinding.rbac.authorization.k8s.io/gitlab-admin created
        ```

        3. Retrive the token for the `gitlab-admin` service account:

        ```bash
        kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
        ```

        Copy the `<authentication_token>` value from the output:

        ```
        Name:         gitlab-admin-token-2d4h8
        Namespace:    kube-system
        Labels:       <none>
        Annotations:  kubernetes.io/service-account.name: gitlab-admin
                    kubernetes.io/service-account.uid: 1294a040-91a0-4e66-a0c8-4652a5d325b4

        Type:  kubernetes.io/service-account-token

        Data
        ====
        ca.crt:     1720 bytes
        namespace:  11 bytes
        token:      <authentication_token>
        ```

    6. **GitLab-managed cluster** - Leave this checked if you want GitLab to manage namespaces and service accounts for this cluster.

    7. **Project namespace** (optional) - You don't have to fill it; by leaving it blank, GitLab will create one for you. Also:

    * Each project should have a unique namespace.
    * The project namespace is not necessarily the namespace of the secret, if you're using a secret with broader permissions, like the secret from `default`.
    * You should **not** use `default` as the project namespace.
    * If you or someone created a secret specifically for the project, usually with limited permissions, the secret's namespace and project namespace may be the same.

4. Finally, click the **Create Kubernetes cluster** button.

After a couple of minutes, your cluster will be ready to go. You can now proceed to install some pre-defined applications.

**For the the demo, the AKS Cluster is RBAC disabled**

# TODOs

- [ ] Add azure storage account for tf state
- [ ] Service Principal
- [x] Notes for Autodevops configuration
- [ ] RBAC Kubernetes cluster
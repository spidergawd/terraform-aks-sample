provider "azurerm" {
  version = "~> 2.28.0"
  features {}
}

locals {
  suffix = "${var.project}${var.increment}"
  common_tags = {
    Environment = "Demo"
  }
}

resource "azurerm_resource_group" "demo" {
  name     = "rg-${local.suffix}"
  location = var.location

  tags = local.common_tags
}

resource "azurerm_kubernetes_cluster" "demo" {
  name                = local.suffix
  location            = azurerm_resource_group.demo.location
  resource_group_name = azurerm_resource_group.demo.name
  dns_prefix          = "${local.suffix}-k8s"

  default_node_pool {
    name       = local.suffix
    node_count = var.node_count
    vm_size    = var.vm_size

    tags = local.common_tags
  }

  identity {
    type = "SystemAssigned"
  }

  addon_profile {
    aci_connector_linux {
      enabled = false
    }

    azure_policy {
      enabled = false
    }

    http_application_routing {
      enabled = false
    }

    kube_dashboard {
      enabled = true
    }

    oms_agent {
      enabled = false
    }
  }

  tags = local.common_tags
}

data "azurerm_public_ip" "demo" {
  name                = reverse(split("/", tolist(azurerm_kubernetes_cluster.demo.network_profile.0.load_balancer_profile.0.effective_outbound_ips)[0]))[0]
  resource_group_name = azurerm_kubernetes_cluster.demo.node_resource_group
}

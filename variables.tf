variable "location" {
  description = "The location where the Managed Kubernetes Cluster should be created."
  type        = string
  default     = "francecentral"
}

variable "project" {
  description = "Client and/or project name shortened"
}

variable "increment" {
  description = "Project increment (rule name)"
  default     = "01"
}

variable "node_count" {
  description = "The initial number of nodes which should exist in this node Pool."
  type        = number
  default     = 1
}

variable "vm_size" {
  type        = string
  description = "The size of the Virtual Machine, such as Standard_DS2_v2"
  default     = "Standard B2ms"
}
